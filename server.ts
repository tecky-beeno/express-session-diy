import express, { Request, Response, NextFunction } from 'express'
import cookieParser from 'cookie-parser'
import { join, resolve } from 'path'

let app = express()

app.use(cookieParser())

function expressSession() {
  let sessions: any = {}
  function newSid() {
    while (true) {
      let sid = Math.random().toString()
      if (sid in sessions) {
        continue
      } else {
        return sid
      }
    }
  }
  return (req: Request, res: Response, next: NextFunction) => {
    console.log(req.method, req.path)
    if (!req.cookies) {
      console.log('request without cookie')
      next()
      return
    }
    let sid: string
    if (req.cookies.sid) {
      sid = req.cookies.sid
      console.log('request with old session, sid:', sid)
    } else {
      sid = newSid()
      console.log('request with new session, sid:', sid)
      res.cookie('sid', sid)
    }
    if (!(sid in sessions)) {
      sessions[sid] = {}
    }
    req.session = sessions[sid]
    console.log('session data:', req.session)
    next()
  }
}

app.use(expressSession())

// counter
app.use((req, res, next) => {
  if (!req.session) {
    next()
    return
  }
  if (!req.session['counter']) {
    req.session['counter'] = 1
  } else {
    req.session['counter']++
  }
  next()
})

app.get('/counter', (req, res) => res.json(req.session['counter']))

app.use(express.static('public'))

app.use((req, res) => {
  res.sendFile(resolve(join('public', '404.html')))
})

let port = 8200
app.listen(port, () => {
  console.log('listening on http://127.0.0.1:' + 8200)
})
