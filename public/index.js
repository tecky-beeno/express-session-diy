console.log('init javascript');

let counter = document.getElementById('counter');
let headCount = document.getElementById('headCount');
let uptime = document.getElementById('uptime');
let startTime = Date.now();

async function inc() {
  let res = await fetch('/counter', {
    method: 'POST',
  });
  let newValue = await res.json();
  counter.textContent = newValue;
}

async function dec() {
  let res = await fetch('/counter', {
    method: 'DELETE',
  });
  let newValue = await res.json();
  counter.textContent = newValue;
}

function formatTime(time) {
  let second = time / 1000;
  second = Math.floor(second);
  return second + 'sec';
}

async function loadData() {
  let now = Date.now();
  let totalTime = now - startTime;
  uptime.textContent = formatTime(totalTime);

  let res = await fetch('/counter');
  res.json = async function () {
    let text = await res.text();
    return JSON.parse(text)
  };
  let value = await res.json();
  counter.textContent = value;

  res = await fetch('/session/count');
  value = await res.json();
  headCount.textContent = value;
}

loadData();
setInterval(loadData, 1000);
